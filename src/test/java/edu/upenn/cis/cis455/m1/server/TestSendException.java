package edu.upenn.cis.cis455.m1.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.mockito.Mockito.*;
import org.mockito.Mockito;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.TestHelper;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpIoHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.Response;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.util.HttpParsing;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m1.server.HttpRequestHandlerExtended;
import edu.upenn.cis.cis455.m1.server.interfaces.WebService;

import org.apache.logging.log4j.Level;

public class TestSendException {
    @Before
    public void setUp() {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
    }
    
    String sampleGetRequest = 
        "GET /a/b/hello.htm?q=x&v=12%200 HTTP/1.1\r\n" +
        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
        "Host: www.cis.upenn.edu\r\n" +
        "Accept-Language: en-us\r\n" +
        "Accept-Encoding: gzip, deflate\r\n" +
        "Cookie: name1=value1;name2=value2;name3=value3\r\n" +
        "Connection: Keep-Alive\r\n\r\n";
        
    String sampleGetRequest2 = 
        "GET /folder/pennEng.gif HTTP/1.1\r\n" +
        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
        "Host: localhost:8080\r\n" +
        "Accept-Language: en-us\r\n" +
        "Accept-Encoding: gzip, deflate\r\n" +
        "Connection: Keep-Alive\r\n\r\n";
        
    String sampleGetRequest3 = 
        "GET http://localhost:8080/index.html HTTP/1.1\r\n" +
        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
        "Host: localhost:8080\r\n" +
        "Accept-Language: en-us\r\n" +
        "Accept-Encoding: gzip, deflate\r\n" +
        "Connection: Keep-Alive\r\n\r\n";
        
     String sampleGetRequest4 = 
        "GET /index.html?name1=value1&name2=value2 HTTP/1.1\r\n" +
        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n" +
        "Host: localhost:8080\r\n" +
        "Accept-Language: en-us\r\n" +
        "Accept-Encoding: gzip, deflate\r\n" +
        "Connection: Keep-Alive\r\n\r\n";
        
    
    @Test
    public void testSendException() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest, 
            byteArrayOutputStream);
        
        HaltException halt = new HaltException(404, "Not found");
        
        HttpIoHandler.sendException(s, null, halt);
        String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
        System.out.println(result);
        
        assertTrue(result.startsWith("HTTP/1.1 404"));
    }

    @Test
    public void testCreateRequest() throws IOException
    {
    	final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest, 
            byteArrayOutputStream);
            
        Request req = HttpIoHandler.getParsedRequest(s, "projects/555-hw1/www");
        
        assertEquals(req.pathInfo(), "/a/b/hello.htm");
        
        
    	//Response obj = ServiceFactory.createRequest(s, uri, keepAlive, headers, parms);
    }
    

    
    @Test
    public void testSendResponse() throws IOException
    {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest2, 
            byteArrayOutputStream);
            Map<String, String> map = new HashMap<String, String>();
//            HttpRequestHandler obj = new HttpRequestHandlerExtended();
//            
        Request req = HttpIoHandler.getParsedRequest(s, "projects/555-hw1/www");
        
        Response resp = new ResponseToClient();
        resp.body("It works!");
        resp.status(200);
        resp.type("text/html");
        map.put("Date", "Wed 28-Sep-17 19:49:27 GMT");
        ((ResponseToClient) resp).headerMap(map);
        HttpIoHandler.sendResponse(s, req, resp);
        
        String result = byteArrayOutputStream.toString("UTF-8").replace("\r", "");
        System.out.println(result);
        
        assertTrue(result.startsWith("HTTP/1.1 200"));
        
    }
    
    @Test
    public void testSingletonWebServiceInstance()
    {
        
        WebService obj1 = WebServiceController.getWebServiceSingleton();
        obj1.port(9800);
        WebService obj2 = WebServiceController.getWebServiceSingleton();
        assertEquals(obj1, obj2); 
    }
    
    @Test
    public void testPathInfoWithHttp() throws IOException
    {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest3, 
            byteArrayOutputStream);
        Request req = HttpIoHandler.getParsedRequest(s, "projects/555-hw1/www");
        
        assertEquals(req.pathInfo(), "/index.html");
    }
    
    @Test
    public void testUriWithParms() throws IOException
    {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Socket s = TestHelper.getMockSocket(
            sampleGetRequest4, 
            byteArrayOutputStream);
        Request req = HttpIoHandler.getParsedRequest(s, "projects/555-hw1/www");
        
        assertEquals(req.pathInfo(), "/index.html");
    }
    
    
    
    
    @After
    public void tearDown() {}
}
