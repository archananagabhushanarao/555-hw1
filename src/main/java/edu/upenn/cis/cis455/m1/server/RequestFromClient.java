package edu.upenn.cis.cis455.m1.server;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RequestFromClient extends Request {
    
    String requestMethod;
    String host;
    String userAgent;
    int port;
    String pathInfo;
    String url;
    String uri;
    String protocol;
    String contentType;
    String ip;
    String body;
    int contentLength;
    Set<String> headers;
    Map<String, String> headerMap;
    Boolean keepAlive;
    
    public RequestFromClient(String requestMethod,
						    String host,
						    String userAgent,
						    int port,
						    String pathInfo,
						    String url,
						    String uri,
						    String protocol,
						    String contentType,
						    String ip,
						    String body,
						    int contentLength,
						    Set<String> headers,
						    Boolean keepAlive,
						    Map<String, String> headerMap)
    {
        
    	this.requestMethod = requestMethod;
    	this.host = host;
    	this.userAgent = userAgent;
    	this.port=port;
    	this.pathInfo=pathInfo;
    	this.url=url;
    	this.uri=uri;
    	this.protocol=protocol;
    	this.contentType=contentType;
    	this.ip=ip;
    	this.body=body;
    	this.contentLength=contentLength;
    	this.headers=headers;
    	this.persistentConnection(keepAlive);
    	this.headerMap=new HashMap<String, String>(headerMap); 
    }
    @Override
    public String requestMethod()
    {
        return requestMethod;
    }

    /**
     * @return The host
     */
    @Override
    public String host()
    {
        return host;
    }
    
    /**
     * @return The user-agent
     */
    @Override
    public  String userAgent()
    {
        return userAgent;
    }
    
    /**
     * @return The server port
     */
    @Override
    public int port()
    {
        return port;
    }
    
    /**
     * @return The path
     */
    @Override
    public String pathInfo()
    {
        return pathInfo;
    }
    
    /**
     * @return The URL
     */
    @Override
    public String url()
    {
        return url;
    }
    
    /**
     * @return The URI up to the query string
     */
    @Override
    public String uri() 
    {
        return uri;
    }
    /**
     * @return The protocol name and version from the request
     */
    @Override
    public String protocol()
    {
        return protocol;
    }


    /**
     * @return The MIME type of the body
     */
    @Override
    public String contentType()
    {
        return contentType;
    }
    
    /**
     * @return The client's IP address
     */
    @Override
    public String ip()
    {
        return ip;
    }
    
    /**
     * @return The request body sent by the client
     */
    @Override
    public String body()
    {
        return body;
    }
    
    /**
     * @return The length of the body
     */
    @Override
    public int contentLength()
    {
        return contentLength;
    }
    
    /**
     * @return Get the item from the header
     */
    @Override
    public String headers(String name)
    {
        if(headerMap.containsKey(name))
        {
        	return headerMap.get(name);
        }
		return null;
    }
    
    @Override
    public Set<String> headers()
    {
        return headers;
    }

}
