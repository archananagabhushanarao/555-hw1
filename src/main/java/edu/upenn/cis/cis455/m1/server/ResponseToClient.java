package edu.upenn.cis.cis455.m1.server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import edu.upenn.cis.cis455.m1.server.interfaces.Response;

public class ResponseToClient extends Response {

	Map<String,String> headerMap;
	String statusReason;
	public ResponseToClient()
	{
		headerMap = new HashMap<String, String>();
	}

	public void addToHeaderMap(String key, String value)
	{
		headerMap.put(key, value);
	}
	
	@Override
	public String getHeaders() {
		String headers = new String();
		

		for (Map.Entry<String, String> entry : headerMap.entrySet()) {
		    String line =entry.getKey() + ": " + entry.getValue();
		    headers += line + "\r\n";
		}
		return headers;
	}
	
	public void formStatusReason()
	{
		switch(this.statusCode) {
		case 200: statusReason = "OK";
				break;
		}
	}
	
	public String statusReason()
	{
		return statusReason;
	}
	
	public void headerMap(Map<String, String> headerMap)
	{
		this.headerMap = headerMap;
		Set keys = headerMap.keySet();

		   for (Iterator i = keys.iterator(); i.hasNext(); ) {
		       String key = (String) i.next();
		       String value = (String) headerMap.get(key);
//		       System.out.println(key + " = " + value);
		   }
	}
}
