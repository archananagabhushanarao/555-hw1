package edu.upenn.cis.cis455.m1.server;
import edu.upenn.cis.cis455.m1.server.interfaces.WebService;
import edu.upenn.cis.cis455.handlers.Route;
import java.io.*;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class WebServiceExtended extends WebService{
    
    int port;
    String ipAddress;
    String directory;
    int threads;
    
    @Override
    public void port(int port)
    {
        this.port = port;
    }

    @Override
    public void ipAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }
    
    @Override
    public void staticFileLocation(String directory)
    {
        this.directory = directory;
        //TO DO: Validate the directory path
    }
    
    @Override
    public void threadPool(int threads)
    {
        this.threads = threads;
    }
    
    @Override
    public void start()
    {
        //Start listening at HTTP Server
        //Fork n threads
        try{
            basicServer = new HttpServer(port, ipAddress, directory, threads);
            basicServer.startHttpServer();
        }
        catch(IOException ie)
        {
            ie.printStackTrace();
        }
        catch(InterruptedException ie)
        {
            ie.printStackTrace();
        }
        
    }
    
    @Override
    public void stop()
    {
        try
        {
        	System.out.println("Shutdown requested");
        	// STOP listening 
	    	basicServer.shutdownServer();
	        
	    	//Check if all threads completed execution
	        
	        /*Map map= Collections.synchronizedMap(basicServer.getControlMap());*/
	        Boolean doneFlag = false;
	        while(doneFlag == false)
	        {
		        synchronized(basicServer.getControlMap()){
		            
		        	for (Map.Entry<Thread, String> entry : basicServer.getControlMap().entrySet())
		        	{
		        	   if(!(entry.getValue().equals("Waiting"))) 
		        	   { 
		        	   doneFlag=false;
		        	   break;
		        	   }
		        	   doneFlag = true;
		        	}
		        	basicServer.getControlMap().notifyAll();
		        	if(doneFlag == false)
		        		basicServer.getControlMap().wait();
		        }
		        
	        }
	        System.out.println("Bye!");
	        System.exit(0);
        }
        catch (InterruptedException ie)
        {
        	ie.printStackTrace();
        	System.out.println("Shutdown Interrupted");
        }
    }
    
    public void notifyDone(HttpWorker worker)
    {
    	basicServer.done(worker);
    }
    public HttpServer getServerInstance()
    {
    	return basicServer;
    }
    @Override
    public void get(String path, Route route){
        //For MS2
    }
    
}
