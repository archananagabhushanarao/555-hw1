package edu.upenn.cis.cis455.m1.server;
import static edu.upenn.cis.cis455.ServiceFactory.createRequestHandlerInstance;
import edu.upenn.cis.cis455.m1.server.WebServiceExtended;
import edu.upenn.cis.cis455.m1.server.WebServiceController;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpIoHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.m1.server.interfaces.Response;
import edu.upenn.cis.cis455.ServiceFactory;

/**
 * Stub class for a thread worker for
 * handling Web requests
 */
public class HttpWorker implements Runnable {
    
    //static final Logger logger = LogManager.getLogger(HttpServer.class);
    private HttpTaskQueue sharedTaskQueue;
    HttpTask task;
    Request request;
    Response response;
    String serverRoot;
    //private final int queueSize;
    
    public HttpWorker(HttpTaskQueue sharedQueue, String directory) {
		this.sharedTaskQueue = sharedQueue;
		this.serverRoot = directory;
		
	}
	
	public void run()
	{
	    while(true)
	    {
	    try{
	    	Boolean shutdown = false;
    	    // Check if queue has anything in it
	    	//System.out.println("Checking again");
    	    task = sharedTaskQueue.readFromQueue();
    	    //System.out.println("Dequeued the task from Queue");
    	    // Dequeue the task
    	    /***** Part of HttpIOHandler *******/
    	    request = HttpIoHandler.getParsedRequest(task.getSocket(), serverRoot);
    	    
    	    // Figure the response
    	    response = ServiceFactory.createResponse();
    	    
    	    Path path = FileSystems.getDefault().getPath(serverRoot);
    	    HttpRequestHandler reqHandler = createRequestHandlerInstance(path);
    	   //Notify Start
    	    ((WebServiceExtended) WebServiceController.getWebServiceSingleton()).getServerInstance().start(this);
        	//Form the response
        	reqHandler.handle(request, response); 
        	if(request.pathInfo().equals("/shutdown"))
        	{
        		shutdown = true;
        	}
    	    //Send response on Socket
        	Boolean keepConnection = HttpIoHandler.sendResponse(task.getSocket(), request, response);
        	
        	if(!keepConnection)
        	{
        		//task.getSocket().getInputStream().close();
        		task.getSocket().close();
        	}
        	//Notify Done
        	
        	((WebServiceExtended) WebServiceController.getWebServiceSingleton()).getServerInstance().done(this);
        	//System.out.println("After updating done");
        	//Start shutdown
        	if(shutdown)
        	{
        		System.out.println("Shutdown request confirmed");
        		WebServiceController.getWebServiceSingleton().stop();
        	}
    	    //Loop forever
        }
        catch(InterruptedException ie)
        {
        	ie.printStackTrace();
        }
        catch(IOException ie)
        {
            ie.printStackTrace();
        }
	    catch (HaltException he)
        {
            try {
            	
            	Boolean keepConnection =HttpIoHandler.sendException(task.getSocket(), request, he);
            	if(!keepConnection)
            	{
            		//task.getSocket().getInputStream().close();
            		task.getSocket().close();
            	}
            }
            catch(IOException ie)
            {
            	ie.printStackTrace();
            }
	    	
        }
       
    }



	}
}


