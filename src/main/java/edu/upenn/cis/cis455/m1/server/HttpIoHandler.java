package edu.upenn.cis.cis455.m1.server;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.io.*;

import javax.servlet.http.HttpServletResponse;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

import static edu.upenn.cis.cis455.ServiceFactory.*;

import edu.upenn.cis.cis455.ServiceFactory;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.m1.server.interfaces.Response;
import edu.upenn.cis.cis455.util.HttpParsing;
import edu.upenn.cis.cis455.m1.server.ResponseToClient;

/**
 * Handles marshalling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    //final static Logger logger = LogManager.getLogger(HttpIoHandler.class);

    /**
     * Sends an exception back, in the form of an HTTP response code and message.  Returns true
     * if we are supposed to keep the connection open (for persistent connections).
     */
    public static boolean sendException(Socket socket, Request request, HaltException except) throws IOException{
        //Form the response line
    	
    	String responseLine = "HTTP/1.1"+" "+ Integer.toString(except.statusCode());
    	if(!responseLine.isEmpty())
		{
		 responseLine +=" "+except.body();
		}
    	responseLine +="\r\n";
    	responseLine += "Content-Type: text/html";
    	
    	responseLine += "\r\n";
    	if(!responseLine.isEmpty())
		{
		    responseLine += "Content-Length: "+Integer.toString(except.body().getBytes().length)+"\r\n";
		     responseLine +="\r\n"+except.body()+"\r\n";
		}
    	responseLine += "\r\n";
    	//Write to socket
    	PrintWriter out =  new PrintWriter(socket.getOutputStream(), true);
    	////System.out.println("Sending response: "+responseLine);
    	out.print(responseLine);
    	out.flush();
    	out.close();
    	//Check if persistent connection
    	return false;
    }

    /**
     * Sends data back.   Returns true if we are supposed to keep the connection open (for 
     * persistent connections).
     */
    public static boolean sendResponse(Socket socket, Request request, Response response) throws IOException {
        
    	String responseString = request.protocol()+" "+response.status()+" "+((ResponseToClient) response).statusReason()+"\r\n";
    	responseString += response.getHeaders();
    	responseString += "Content-Type: "+response.type()+"\r\n";
    	responseString += "\r\n";
    	if(response.type().contains("text") && !request.pathInfo().equals("/control"))
    	{
    		if(!request.requestMethod().equals("HEAD") )
    		{
    		//String format
    			responseString += response.body()+"\r\n";
    		}
    		PrintWriter out =  new PrintWriter(socket.getOutputStream(), true);
    		
        	////System.out.println("Sending response: "+responseString);
        	out.print(responseString);
        	out.flush();
        	out.close();
    	}
    	else
    	{
    		PrintWriter out =  new PrintWriter(socket.getOutputStream(), true);
        	////System.out.println("Sending response: "+responseString);
        	out.print(responseString);
        	out.flush();
        	if(!request.requestMethod().equals("HEAD"))
    		{
	        	OutputStream socketOutputStream = socket.getOutputStream();
	        	socketOutputStream.write(response.bodyRaw());
	        	socketOutputStream.close();
	        	out.flush();
    		}
        	out.close();
    	}
    	
    	//Send the response
    	return false;
    }
    
    public static Request getParsedRequest(Socket socket, String serverDirectory) throws IOException, HaltException
    {
        Request request=null;
        
        InputStream input = socket.getInputStream();
        Map<String, String> headers =  new HashMap<String, String> ();
        Map<String, List<String>> parms = new LinkedHashMap<String, List<String>>();
        
        String remoteIP;
        if(socket.getInetAddress() == null)
            remoteIP = "127.0.0.1";
        else
            remoteIP = socket.getInetAddress().getHostAddress();
        
        String uri = HttpParsing.parseRequest(
                    remoteIP, 
                    input, 
                    headers,
                    parms);
         
        ////System.out.println("uri " + uri);
        
        boolean keepAlive = true;
        headers.put("serverRoot", serverDirectory);
    	request = ServiceFactory.createRequest(socket,
                         uri,
                         keepAlive,
                         headers,
                         parms);
    	
    	for(String s:request.headers())
    	{
    		//System.out.println("key= "+s +" value= "+ request.headers(s));
    	}
    	
        return request;
    }


    
}
