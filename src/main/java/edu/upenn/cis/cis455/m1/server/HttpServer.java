package edu.upenn.cis.cis455.m1.server;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import edu.upenn.cis.cis455.m1.server.HttpWorker;
//import java.lang.InterruptedException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.net.*;
import java.io.*;
/**
 * Stub for your HTTP server, which
 * listens on a ServerSocket and handles
 * requests
 */
public class HttpServer implements ThreadManager {

    //static final Logger logger = LogManager.getLogger(HttpServer.class);
    private HttpTaskQueue sharedTaskQueue;
    ServerSocket serverSocket;
    int threads;
    int port;
    String ipAddress;
    String directory;
    int maxTaskQueueSize;
    Boolean shutdown;
    final Map<Thread, String> controlMap;
        
    public HttpServer (int port, String ipAddress, String directory, int threads) throws IOException
    {
       this.port = port;
       serverSocket = new ServerSocket(port);
       this.ipAddress = ipAddress;
       this.directory = directory;
       this.threads = threads;
       this.maxTaskQueueSize = threads;
       shutdown = false;
       controlMap = new HashMap<Thread,String>();
    }
    
    public void addTaskToQueue(Socket socket) throws InterruptedException
    {
        //Create a task
        //logger.info("[Output from log4j] Creating new task with socket");
        HttpTask task = new HttpTask(socket);
        sharedTaskQueue.addToQueue(task);
    }
    
    public void startHttpServer() throws IOException, InterruptedException
    {
        //Fork all the threads
        
        sharedTaskQueue = new HttpTaskQueue(maxTaskQueueSize);
        for(int i =0; i<this.threads;i++)
        {
            Runnable HttpWorkerObj = new HttpWorker(sharedTaskQueue, directory);
            Thread workerThread = new Thread(HttpWorkerObj);
            synchronized(controlMap) {
    	    	controlMap.put(workerThread, "Waiting");
    	    	controlMap.notifyAll();
        	}
            workerThread.start();
            //System.out.println("Created"+ i+"th thread");
        }
        //Start listening
        listenToSocket();
    }
    
        
    private void listenToSocket() throws IOException, InterruptedException
    {
        //logger.info("[Output from log4j] Creating new task with socket");
        //On accepting connection add to queue and keep listening
        while(true)
        {
        	if(isActive())
        	{
        	Socket accepted_socket = serverSocket.accept();
            addTaskToQueue(accepted_socket);
        	}
        	else {
        		serverSocket.close();
        		//Close socket and wait for shutdown
        	}
        }  

        
    }

    
    @Override
    public HttpTaskQueue getRequestQueue() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isActive() {
        
        return !shutdown;
    }

    @Override
    public void start(HttpWorker worker) {
    	System.out.println("Updating start");
    	synchronized(controlMap) {
	    	controlMap.put(Thread.currentThread(), worker.request.url());
	    	controlMap.notifyAll();
    	}
    	System.out.println("Updated start");
    }

    @Override
    public void done(HttpWorker worker) {
    	System.out.println("Updating done");
    	synchronized(controlMap) {
    		controlMap.put(Thread.currentThread(),"Waiting");
    		controlMap.notifyAll();
    	}
    	System.out.println("Updated done");
    }

    public final Map<Thread, String> getControlMap()
    {
    	Map<Thread,String> temp;
    	synchronized(controlMap) {
    		temp = controlMap;
    		controlMap.notifyAll();
    	}
    	return temp;
    }
   
    @Override
    public void error(HttpWorker worker) {
        // TODO Auto-generated method stub
        
    }
    public void shutdownServer()
    {
    	
    	shutdown = true;
    }
}
