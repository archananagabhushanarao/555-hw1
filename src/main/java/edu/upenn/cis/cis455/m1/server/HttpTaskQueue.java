package edu.upenn.cis.cis455.m1.server;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;

import edu.upenn.cis.cis455.m1.server.HttpTask;
/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
     private final ArrayList<HttpTask> sharedQueue;
     private final int queueSize;
     static final Logger logger = LogManager.getLogger(HttpServer.class);
     
     public HttpTaskQueue(int size) {
		sharedQueue = new ArrayList<HttpTask>();
		this.queueSize = size;
	}
    
    public ArrayList<HttpTask> getSharedQueue() {
        return sharedQueue;
    }
   
    public void addToQueue(HttpTask newTask) throws InterruptedException {
        
        
        while (sharedQueue.size() == queueSize) {
    		// Synchronizing on the sharedQueue to make sure no more than one
    		// thread is accessing the queue same time.
    		synchronized (sharedQueue) {
    			logger.info("[Output from log4j] Queue is full!");
    			sharedQueue.wait();
    			// We use wait as a way to avoid polling the queue to see if
    			// there was any space for the producer to push.
    		}
		}

	    //Adding element to queue and notifying all waiting consumers
    	synchronized (sharedQueue) {
    		logger.info("[Output from log4j] Adding task to queue");
    		sharedQueue.add(newTask);
    		sharedQueue.notifyAll();
    	}
	}
	
	public HttpTask readFromQueue() throws InterruptedException {
		
		while (sharedQueue.isEmpty()) {
			//If the queue is empty, we push the current thread to waiting state. Way to avoid polling.
			synchronized (sharedQueue) {
				logger.info("[Output from log4j] Queue is currently empty ");
				sharedQueue.wait();
			}
		}
		
	   //Otherwise consume element and notify waiting producer
		synchronized (sharedQueue) {
			sharedQueue.notifyAll();
			logger.info("[Output from log4j] Removing item from queue ");
			return (HttpTask) sharedQueue.remove(0);
		}
	}
}
