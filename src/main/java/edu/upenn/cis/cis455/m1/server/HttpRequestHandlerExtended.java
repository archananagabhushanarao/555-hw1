package edu.upenn.cis.cis455.m1.server;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.m1.server.interfaces.Response;
import edu.upenn.cis.cis455.util.HttpParsing;
import edu.upenn.cis.cis455.m1.server.ResponseToClient;
import edu.upenn.cis.cis455.m1.server.WebServiceExtended;

public class HttpRequestHandlerExtended implements HttpRequestHandler{

	Path serverRoot;
	public HttpRequestHandlerExtended(Path serverRoot)
	{
		this.serverRoot=serverRoot;
	}
	public HttpRequestHandlerExtended()
	{
		
	}
	@Override
	public void handle(Request request, Response response) throws HaltException {
		
		Map<String, String> headerMap = new HashMap<String, String>();
		//Validate the pathInfo
		try {
			
			if(request.pathInfo().equals("/shutdown"))
			{
				//Form response
				response.status(200);
				((ResponseToClient) response).formStatusReason();
				String body ="Server Shutting Down";
				response.body(body);
				
				response.type("text/html");
				
				headerMap.put("Content-Length", Integer.toString(body.getBytes().length));
								
				Date date = new Date();
			    DateTimeFormatter format = DateTimeFormatter.ofPattern("EEE, dd-MMM-yy HH:mm:ss");
		        LocalDateTime ldt = LocalDateTime.parse(date.toString(), DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.US));
		        
		        ZonedDateTime utc = ZonedDateTime.of(ldt, ZoneOffset.UTC);
		        //system.out.println("Date: "+format.format(utc));
		        
			    headerMap.put("Date", format.format(utc)+" GMT" );
			    
			    ((ResponseToClient) response).headerMap(headerMap);
				
				return;
			}
			
			if(request.pathInfo().equals("/control"))
			{
				//Form response
				response.status(200);
				((ResponseToClient) response).formStatusReason();
				String body ="";
				//Has to contain map contents
				String responseLine = formTable
						(((WebServiceExtended) WebServiceController.getWebServiceSingleton()).getServerInstance().getControlMap());
				
				response.bodyRaw(responseLine.getBytes());
				
				response.type("text/html");
				
				headerMap.put("Content-Length", Integer.toString(responseLine.getBytes().length));
				
				
				Date date = new Date();
			    DateTimeFormatter format = DateTimeFormatter.ofPattern("EEE, dd-MMM-yy HH:mm:ss");
		        LocalDateTime ldt = LocalDateTime.parse(date.toString(), DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.US));
		        
		        ZonedDateTime utc = ZonedDateTime.of(ldt, ZoneOffset.UTC);
		        //system.out.println("Date: "+format.format(utc));
		        
			    headerMap.put("Date", format.format(utc)+" GMT" );
			    
			    ((ResponseToClient) response).headerMap(headerMap);
				
				return;
			}
			
			String absolutePath = 
					FileSystems.getDefault().getPath(request.headers("serverRoot")+request.pathInfo()).normalize().toAbsolutePath().toString();
			//system.out.println("absolutePath: "+ absolutePath);
			String serverAbsolutePath = FileSystems.getDefault().getPath(request.headers("serverRoot")).normalize().toAbsolutePath().toString();
			if(!absolutePath.contains(serverAbsolutePath))
			{
				//system.out.println("Invalid path access");
				throw new HaltException(403, "Forbidden");
			}
			
			File requestedFile = new File(absolutePath);
			if(requestedFile.exists() && !requestedFile.isDirectory()) { 
			    //system.out.println("File exists");
			    
			    String mime = HttpParsing.getMimeType(absolutePath);
			    //headerMap.put("Content-type", mime);
			    response.type(mime);
			    Path path = Paths.get(absolutePath);
			    byte[] data = Files.readAllBytes(path);
			    //system.out.println("Lastmodified: "+requestedFile.lastModified());
			    Date lmdate = new Date(requestedFile.lastModified());
			    DateTimeFormatter lm_format = DateTimeFormatter.ofPattern("EEE, dd-MMM-yy HH:mm:ss");
		        LocalDateTime lm_ldt = LocalDateTime.parse(lmdate.toString(), DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.US));
		        
		        ZonedDateTime lm_utc = ZonedDateTime.of(lm_ldt, ZoneOffset.UTC);
		        //system.out.println("Date: "+lm_format.format(lm_utc));
		        headerMap.put("Last-Modified:", lm_format.format(lm_utc)+" GMT" );
			    
			    headerMap.put("Content-Length", Long.toString(requestedFile.length()));
			    if(mime.contains("text"))
			    {
			    	//system.out.println("Reading file as string");
			    	String body = new String(data);
			    	response.body(body);
			    }
			    else
			    {
			    	//system.out.println("Reading file as bytes");
			    	response.bodyRaw(data);
			    }
			    //if(!request.persistentConnection())
			    headerMap.put("Connection", "Closed");
			    
			    Date date = new Date();
			    DateTimeFormatter format = DateTimeFormatter.ofPattern("EEE, dd-MMM-yy HH:mm:ss");
		        LocalDateTime ldt = LocalDateTime.parse(date.toString(), DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.US));
		        
		        ZonedDateTime utc = ZonedDateTime.of(ldt, ZoneOffset.UTC);
		        //system.out.println("Date: "+format.format(utc));
		        
			    headerMap.put("Date", format.format(utc)+" GMT" );
			    
			    

			}
			else
			{
				throw new HaltException(404, "Not Found");
			}
			response.status(200);
			((ResponseToClient) response).formStatusReason();
			((ResponseToClient) response).headerMap(headerMap);
			//Form the status-line
			
			//Form Headers
			//Content-length
			//Date
			//
			//getMimeType
		}
		catch(IOException ie)
		{
			
		}
	}
	
	private String formTable(Map<Thread, String> controlMap)
	{
		String html ="<html>";
		html += "<body>";
		html += "<table style=\"width:100%\">";
		html +="<tr>";
		html += "<th>Thread</th>";
		html += "<th>Status</th>";
		html +="</tr>";
		synchronized (controlMap) {
			for (Entry<Thread, String> entry : controlMap.entrySet()) {
			    
				html +="<tr>";
				html += "<th>"+ entry.getKey().getName()+"</th>";
				html += "<th>"+ entry.getValue() + "</th>";
				html +="<\tr>";
			}
		controlMap.notifyAll();
		}
		html += "</table>";
				
		html += "<form method=\"get\""; 
		html += "action=\"/shutdown\">";
		html += "<Input type = \"submit\""; 
		html += "name=\"Shutdown\"";
		html += "value=\"Shutdown\">";
		html += "</form>";
		html += "</body>";
		html += "</html>";
		
		return html;
	}
	
}
