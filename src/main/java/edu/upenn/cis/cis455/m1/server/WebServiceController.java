package edu.upenn.cis.cis455.m1.server;
import edu.upenn.cis.cis455.m1.server.interfaces.WebService;
import static edu.upenn.cis.cis455.ServiceFactory.*;
import edu.upenn.cis.cis455.handlers.Route;
import java.io.*;

public class WebServiceController {
    
    static WebService webServiceObject;
    
    public WebServiceController ()
    {
        
    }
    
    public static WebService getWebServiceSingleton()
    {
        if (webServiceObject == null)
        {
        	webServiceObject = getServerInstance();
        }
        return webServiceObject;
    }
    
    public static void port(int port)
    {
        getWebServiceSingleton().port(port);
    }

    public static void ipAddress(String ipAddress)
    {
        getWebServiceSingleton().ipAddress(ipAddress);
    }
      
    public static void staticFileLocation(String directory)
    {
        getWebServiceSingleton().staticFileLocation(directory);
    }
     
    public static void threadPool(int threads)
    {
        getWebServiceSingleton().threadPool(threads);
    }
    
    public static void start()
    {
        getWebServiceSingleton().start();
    }
}
