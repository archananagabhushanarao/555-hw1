
package edu.upenn.cis.cis455;

import org.apache.logging.log4j.Level;
import static edu.upenn.cis.cis455.m1.server.WebServiceController.*;
//import org.apache.logging.log4j.core.config.Configurator;

public class WebServer {
    public static void main(String[] args) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        
        int port = 8080;
        String directory = "/www";

        if(!(args[0].equals("")) && !(args[1].equals("")))
		{
		    port = Integer.parseInt(args[0]);
		    directory = args[1];
		}
        
        port(port);
        ipAddress("127.0.0.1");
        staticFileLocation(directory);
        threadPool(250);
        start();
        
        //System.out.println("Waiting to handle requests!");
    }

}
