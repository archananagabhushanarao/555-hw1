 package edu.upenn.cis.cis455;

import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.*;
import java.io.*;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m1.server.interfaces.WebService;
import edu.upenn.cis.cis455.m1.server.interfaces.HttpRequestHandler;
import edu.upenn.cis.cis455.m1.server.interfaces.Request;
import edu.upenn.cis.cis455.m1.server.interfaces.Response;
import edu.upenn.cis.cis455.m2.server.interfaces.Session;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.server.HttpRequestHandlerExtended;
import edu.upenn.cis.cis455.m1.server.RequestFromClient;
import edu.upenn.cis.cis455.m1.server.ResponseToClient;
import edu.upenn.cis.cis455.m1.server.WebServiceExtended;

public class ServiceFactory {

    
    final static Logger logger = LogManager.getLogger(ServiceFactory.class);
    /**
     * Get the HTTP server associated with port 8080
     */
    public static WebService getServerInstance() {
    	
    	logger.debug("Creating WebService Instance");
    	WebService webServiceObject = new WebServiceExtended();
    	return webServiceObject;
    }
    
    /**
     * Create an HTTP request given an incoming socket
     */
    public static Request createRequest(Socket socket,
                         String uri,
                         boolean keepAlive,
                         Map<String, String> headers,
                         Map<String, List<String>> parms) throws HaltException {
        
        String requestMethod="";
        String host="";
        String userAgent="";
        int port=0;
        String pathInfo="";
        String url="";
        String protocol="";
        String contentType="";
        String ip="";
        String body="";
        int contentLength=0;
        String new_uri="";
        Set<String> new_headers;
        
        //Validate the request
        //System.out.println("In createRequest");
        
        if(headers.containsKey("Method"))
        {
            requestMethod = headers.get("Method");
            switch(requestMethod)
            {
                case "GET": 
                            break;
                case "HEAD": 
                            break;
                default: {
                	logger.debug("Bad Method");
                	throw new HaltException(400, "Bad Method");
                }
            }
            //System.out.println("requestMethod "+requestMethod);
        }
//        else
//        {
//            logger.debug("No Method");
//            throw new HaltException(400);            
//        }
        if(headers.containsKey("protocolVersion"))
        {
            protocol = headers.get("protocolVersion");
            switch(protocol)
            {
                case "HTTP/1.1": 
                            break;
                default: throw new HaltException(505, "HTTP Version Not Supported");
            }
            //System.out.println("protocol "+protocol );
        }
        else
        {
            logger.error("No protocolVersion");
            throw new HaltException(400, "Bad Request without protocol");
        }
        
        if(headers.containsKey("host"))
        {
            host = headers.get("host");
            //System.out.println("host "+host);
        }
        else
        {
                logger.error("No host");
                throw new HaltException(400, "Host must be present");  
        }
        if(headers.containsKey("http-client-ip"))
        {
            if(socket.getInetAddress() == null)
            {
                ip = "127.0.0.1";
            }
            else
               ip = socket.getInetAddress().getHostAddress();
        }
        else
        {
//            logger.error("No http-client-ip");
//            throw new HaltException(400);
        }
        //System.out.println("IP: "+ip);
        if(headers.containsKey("user-agent"))
        {
            userAgent = headers.get("user-agent");
        }
        else
        {
//            logger.error("No http-client-ip");
//            throw new HaltException(400);
        }
        if(headers.containsKey("connection"))
        {
            if(headers.get("connection").equals("keep-alive"))
            	keepAlive = true;
            else
            	keepAlive=false;
        }
        else
        {
//            logger.error("No http-client-ip");
//            throw new HaltException(400);
        }
        port = socket.getLocalPort();
        //System.out.println("port" + port);
        //Uri, requstPath and url
        if(uri.startsWith("http://") )
        {
        	uri = uri.substring(7);
        	//System.out.println("NOW URI "+uri);
        	uri = uri.substring(uri.indexOf("/"));
        	//System.out.println("NOW URI "+uri);
        }
        if(parms.isEmpty())
        	pathInfo = uri;
        else
        {
            String[] subs = uri.split("\\?");
            pathInfo = subs[0];
        }
        new_uri = "http://" + host+pathInfo;
        
        url = "http://" + host+uri;
         
        //System.out.println("uri "+ new_uri + " url " + url +  " pathInfo "+pathInfo);
        new_headers = new HashSet<String>();
       
       Iterator<Map.Entry<String, String>> it = headers.entrySet().iterator();
       while (it.hasNext()) {
           Map.Entry<String, String> pair = (Map.Entry<String, String>)it.next();
           
           new_headers.add(pair.getKey());
           
           //it.remove(); // avoids a ConcurrentModificationException
       }
       for (String s : new_headers) {
    	   ////System.out.println("Key "+s);
    	}
       
    
        //Create a Request type object
        Request request = new RequestFromClient(requestMethod,
										        host,
										        userAgent,
										        port,
										        pathInfo,
										        url,
										        uri,
										        protocol,
										        contentType,
										        ip,
										        body,
										        contentLength,
										        new_headers,
										        keepAlive,
										        headers);
        //Extract the correct parameters and fill in the object variables
        return request;
    }
    
    /**
     * Gets a request handler for files (i.e., static content) or dynamic content
     */
    public static HttpRequestHandler createRequestHandlerInstance(Path serverRoot) {
    	HttpRequestHandler reqHandler = new HttpRequestHandlerExtended(serverRoot);
    	return reqHandler;
    }

    /**
     * Gets a new HTTP Response object
     */
    public static Response createResponse() {
        Response response = new ResponseToClient();
		return response;
    }

    /**
     * Creates a blank session ID and registers a Session object for the request
     */
    public static String createSession() {
        return null;
    }
    
    /**
     * Looks up a session by ID and updates / returns it
     */
    public static Session getSession(String id) {
        
        return null;
    }
}